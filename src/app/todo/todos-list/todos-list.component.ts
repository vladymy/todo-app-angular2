import {Component, OnInit} from '@angular/core';
import {TodoDataService} from '../_services/todo-data.service';
import {TodosList} from '../_models/todos-list';

@Component({
    selector: 'todos-list',
    templateUrl: './todos-list.component.html',
    styleUrls: ['./todos-list.component.scss'],
    providers: [TodoDataService]
})
export class TodosListComponent implements OnInit{

    public todos: TodosList[];

    constructor(private todoDataService: TodoDataService) {
    }

    ngOnInit() {
        this.todos = this.todoDataService.getAllTodos();
    }

    updateCheckedTodo(todo: TodosList) {
        this.todoDataService.updateTodo(todo);
    }

    removeTodo(todo: TodosList): void {
        this.todoDataService.deleteTodoById(todo.id);
        this.todos = this.todoDataService.getAllTodos();
    }

}