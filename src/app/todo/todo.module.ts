import {NgModule} from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {
    MatInputModule,
    MatListModule,
    MatCardModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule,
    MatDialogModule
} from '@angular/material';

import {TodoRoutingModule} from './todo-routing.module';
import {TodosListComponent} from './todos-list/todos-list.component';
import {TodoPopupComponent, TodoPopupDialogComponent} from './todo-modal/todo-dialog.component';


@NgModule({
    imports: [
        BrowserAnimationsModule,
        FormsModule,
        MatInputModule,
        MatListModule,
        MatCardModule,
        MatButtonModule,
        MatCheckboxModule,
        MatIconModule,
        MatDialogModule,
        TodoRoutingModule
    ],
    declarations: [TodosListComponent, TodoPopupComponent, TodoPopupDialogComponent],
    entryComponents: [TodoPopupDialogComponent]
})
export class TodoModule {
}
