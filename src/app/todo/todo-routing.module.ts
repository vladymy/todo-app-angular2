import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {TodosListComponent} from './todos-list/todos-list.component'
import {TodoPopupComponent} from './todo-modal/todo-dialog.component';

const routes: Routes = [
    {
        path: 'todos-list',
        component: TodosListComponent,
        children: [
            {
                path: 'add-todo',
                component: TodoPopupComponent
            }
        ]
    },
    {
        path: 'todo',
        component: TodosListComponent,
        children: [
            {
                path: ':id',
                component: TodoPopupComponent,
                children: [
                    {
                        path: 'edit-todo',
                        component: TodoPopupComponent
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(
            routes,
            { enableTracing: true }
        )
    ],
    exports: [
        RouterModule
    ]
})
export class TodoRoutingModule {
}
