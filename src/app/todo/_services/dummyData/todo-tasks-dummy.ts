export class TodoTasksDummyData {
    id: number = 0;
    name: string = '';
    complete: boolean = false;

    constructor(values: Object = {}) {
        let dummyData = [];
        for (let i = 0; i < 5; i++) {
            dummyData.push({
                id: i,
                title: 'test'+i,
                complete: !!(i%2)
            });
        }

        Object.assign(this, dummyData);
    }

}