import {TodoTasksDummyData} from './todo-tasks-dummy';
export class TodosListDummyData {
    id: number;
    name: string = '';
    todoTasks = [];

    constructor(values: Object = {}) {
        let dummyData = [];
        for (let i = 0; i <10; i++) {
            dummyData.push({
                id: i,
                name: 'test'+i,
                todoTasks: new TodoTasksDummyData()
            });
        }
        Object.assign(this, dummyData);
    }

}