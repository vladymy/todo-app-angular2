import {Injectable} from '@angular/core';
import {TodosList} from '../_models/todos-list';
import {TodoTasks} from '../_models/todo-tasks';

@Injectable()
export class TodoDataService {
    public todoslist: TodosList[];

    constructor() {
        this.todoslist = JSON.parse(localStorage.getItem('todoslist')) || [];
    }

    // Simulate POST /todo
    addTodo(todo: TodosList) {
        if (!todo.id) {
            todo.id = this.todoslist.length;
        }
        this.todoslist.push(todo);
        localStorage.setItem('todoslist', JSON.stringify(this.todoslist));
        return this.todoslist;
    }

    // Simulate PUT /todo/:id
    updateTodo(todo: TodosList) {
        this.todoslist.map(todoitem => {
            if (todoitem.id == todo.id) {
                this.todoslist[todoitem.id] = todo
            }
        });
        localStorage.setItem('todoslist', JSON.stringify(this.todoslist));
        return this.todoslist;
    }

    // Simulate DELETE /todo/:id/
    deleteTodoById(todoId: number) {
        this.todoslist.map((todoItem, index) => {
            if (todoItem.id == todoId) {
                this.todoslist.splice(index, 1);
            }
        });
        localStorage.setItem('todoslist', JSON.stringify(this.todoslist));
        return this.todoslist;
    }

    // Simulate POST /todo/:id/task
    addTodoTask(todo: TodosList, todoTask: TodoTasks) {
        todoTask.id = todo.todoTasks.length;
        todo.todoTasks.push(todoTask);
        return this.todoslist;
    }

    // Simulate DELETE /todo/:id/task/:id
    deleteTodoTaskById(todoId: number, todoTaskId: number) {
        this.todoslist.map((todoItem) => {
            if (todoItem.id == todoId) {
                todoItem.todoTasks.map((todoTaskItem, index) => {
                    if(todoTaskId == index) {
                        this.todoslist[todoId].todoTasks.splice(index, 1);
                    }
                });
            }
        });
        localStorage.setItem('todoslist', JSON.stringify(this.todoslist));
        return this.todoslist;
    }

    // Simulate GET /todo/:id/
    getTodoById(selectedTodoId: number): TodosList {
      return this.todoslist
          .filter(todo => todo.id === selectedTodoId)
          .pop();
    }

    // Simulate GET /todos
    getAllTodos(): TodosList[] {
        this.todoslist = JSON.parse(localStorage.getItem('todoslist')) || [];
        return this.todoslist;
    }

}