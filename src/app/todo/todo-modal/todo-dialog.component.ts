import {Component, Inject, OnInit} from "@angular/core";
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import 'rxjs/add/operator/switchMap';

import {MatDialog, MatDialogRef, MatDialogConfig, MAT_DIALOG_DATA} from "@angular/material";

import {TodosList} from '../_models/todos-list';
import {TodoTasks} from '../_models/todo-tasks';
import {TodoDataService} from '../_services/todo-data.service';

@Component({
    selector: 'todo-popup',
    template: '',
    providers: [TodoDataService]
})
export class TodoPopupComponent {

    dialogRef: MatDialogRef<TodoPopupDialogComponent>;

    config = {
        disableClose: false,
        hasBackdrop: true,
        backdropClass: '',
        width: '',
        height: '',
        position: {
            top: '',
            bottom: '',
            left: '',
            right: ''
        },
        data: ''
    };

    constructor(
        private dialog: MatDialog,
        private todoDataService: TodoDataService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        let todoId;
        this.route.params.subscribe(
            (params) => {
                todoId = parseInt(params['id']);
            });

        this.dialogRef = this.dialog.open(TodoPopupDialogComponent, {
            data: this.todoDataService.getTodoById(todoId) || null
        });
        this.dialogRef.afterClosed().subscribe((result: string) => {
            let todoData = this.todoDataService.getTodoById(todoId) || null;
            if(todoData) {
                this.router.navigate(['todos-list']);
            } else {
                this.router.navigate(['todo']);
            }

        });

    }
}

@Component({
    selector: 'todo-popup-dialog',
    templateUrl: './todo-dialog.html',
    providers: [TodoDataService]
})
export class TodoPopupDialogComponent {
    todo: TodosList = new TodosList();
    newTodoTasks: TodoTasks = new TodoTasks();
    editMode: boolean;

    constructor(
        private todoDataService: TodoDataService,
        public dialogRef: MatDialogRef<TodoPopupDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {

        this.editMode = this.data;
        if(this.editMode) {
            this.todo = this.data;
        }
    }

    updateTodo() {
        if (this.editMode) {
            this.todoDataService.updateTodo(this.todo);
        } else {
            this.todoDataService.addTodo(this.todo);
        }
        this.dialogRef.close();
    }

    addTodoTask() {
        if (this.newTodoTasks.title) {
            this.todoDataService.addTodoTask(this.todo, this.newTodoTasks);
            this.newTodoTasks = new TodoTasks();
        }
    }

    removeTodoTask(selectedTaskId: number) {
        this.todoDataService.deleteTodoTaskById(this.todo.id, selectedTaskId);
        this.todo = this.todoDataService.getTodoById(this.todo.id);
    }

    save() : void {
        this.updateTodo();
    }

    get todos() {
        return this.todoDataService.getAllTodos();
    }
}