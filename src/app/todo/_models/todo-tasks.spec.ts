import { TodoTasks } from './todo-tasks';

describe('Todo', () => {
  it('should create an instance', () => {
    expect(new TodoTasks()).toBeTruthy();
  });
  it('should accept values in the constructor', () => {
    let todo = new TodoTasks ({
      id: 1,
      title : 'hello',
      complete: true
    });
    expect(todo.id).toBe(1);
    expect(todo.title).toBe('hello');
    expect(todo.complete).toEqual(true);
  });
});
