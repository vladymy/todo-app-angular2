export class TodosList {
    id: number;
    name: string = '';
    todoTasks = [];

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}
