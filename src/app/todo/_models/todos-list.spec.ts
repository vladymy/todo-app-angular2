import { TodosList } from './todos-list';

describe('Todo', () => {
  it('should create an instance', () => {
    expect(new TodosList()).toBeTruthy();
  });
  it('should accept values in the constructor', () => {
    let todosList = new TodosList ({
      id: 1,
      name : 'hello',
      todoTasks: [{
        id: 0,
        title: 'test',
        complete: false
      }]
    });
    expect(todosList.id).toBe(1);
    expect(todosList.name).toBe('hello');
    expect(todosList.todoTasks).toEqual([{
      id: 0,
      title: 'test',
      complete: false
    }]);
  });
});
