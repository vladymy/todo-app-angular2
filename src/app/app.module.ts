import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router'

import {AppComponent} from './app.component';
import {TodoModule} from './todo/todo.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: '', redirectTo: 'todos-list', pathMatch: 'full'
            }, {
                path: '**', redirectTo: 'todos-list', pathMatch: 'full'
            }
        ]),
        TodoModule
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
